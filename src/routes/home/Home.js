/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedRelative } from 'react-intl';
import { DatePicker } from 'antd';
import antd from 'antd/lib/date-picker/style/index.css';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';

import Menu from './Menu';

class Home extends React.Component {
  static propTypes = {
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <DatePicker />
          <Menu />
          <h1>Dashboard</h1>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Home);
